<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Bookmark;
use Faker\Generator as Faker;

$factory->define(Bookmark::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'url' => 'https://fabiopacifici.com',
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
    ];
});
