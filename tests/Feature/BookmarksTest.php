<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BookmarksTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function guests_cannot_view_a_bookmark()
    {
        $this->get('/bookmarks')->assertRedirect('/login');
    }

    /** @test  */
    public function guests_cannot_view_the_bookmarks_dashboard()
    {
        $this->get('/home')->assertRedirect('/login');
    }

    /** @test  */
    public function guests_cannot_create_a_bookmark()
    {
        $this->get('/bookmarks/create')->assertRedirect('/login');
    }

    /** @test */
    public function guests_cannot_save_bookmarks()
    {

        //$this->withoutExceptionHandling();
        $user = factory('App\User')->create();
        $this->actingAs($user);
        $attributes = factory('App\Bookmark')->raw();

        $this->post('/bookmarks', $attributes);
        $this->assertDatabaseMissing('bookmarks', $attributes);
    }

    /** @test */
    public function only_authenticated_users_can_save_bookmarks()
    {
        $this->withoutExceptionHandling();
        $user = factory('App\User')->create();
        $this->actingAs($user);

        $attributes = factory('App\Bookmark')->raw(['user_id' => $user->id]);

        $this->post('/bookmarks', $attributes);
        $this->assertDatabaseHas('bookmarks', $attributes);
    }

    /** @test  */
    public function authenticated_users_can_view_their_bookmarks()
    {
        $user = factory('App\User')->create();
        $this->actingAs($user);
        $bookmark = $user->bookmarks()->create(factory('App\Bookmark')->raw());

        $this->post('/bookmarks', $bookmark->toArray());

        $this->get('/home')->assertSee($bookmark->url);
    }

    /** @test  */
    public function authenticated_users_cannot_view_bookmarks_of_others()
    {
        $user = factory('App\User')->create();
        $this->actingAs($user);
        $bookmark = factory('App\Bookmark')->create();

        $this->post('/bookmarks', $bookmark->toArray());
        $this->get($bookmark->path())->assertStatus(403);
    }

    /** @test  */
    public function a_bookmark_requires_a_name()
    {
        $user = factory('App\User')->create();
        $this->actingAs($user);
        $attributes = factory('App\Bookmark')->raw(['name' => '']);
        $this->post('/bookmarks', $attributes)->assertSessionHasErrors('name');
    }

    /** @test  */
    public function a_bookmark_requires_an_url()
    {
        $user = factory('App\User')->create();
        $this->actingAs($user);
        $attributes = factory('App\Bookmark')->raw(['url' => '']);
        $this->post('/bookmarks', $attributes)->assertSessionHasErrors('url');
    }

    /** @test  */
    public function a_bookmark_url_must_be_https()
    {
        $user = factory('App\User')->create();
        $this->actingAs($user);
        $attributes = factory('App\Bookmark')->raw(['url' => 'http://fabiopacifici.com']);
        $this->post('/bookmarks', $attributes)->assertSessionHasErrors('url');
    }

    /** @test  */
    public function a_user_can_delete_a_bookmark()
    {
        $this->withoutExceptionHandling();
        $user = factory('App\User')->create();
        $this->actingAs($user);
        $bookmark = factory('App\Bookmark')->create();

        $this->delete('/bookmarks/'.$bookmark->id);
        $this->assertDatabaseMissing('bookmarks', ['name' => $bookmark->name, 'url' => $bookmark->url]);
    }
}
