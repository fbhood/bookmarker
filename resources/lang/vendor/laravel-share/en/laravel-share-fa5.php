<?php

return [
    'facebook' => '<a href=":url" class="social-button :class" id=":id" title=":title"><span class="fab fa-facebook-square fa-3x"></span></a>',
    'twitter' => '<a href=":url" class="social-button :class" id=":id" title=":title"><span class="fab fa-twitter-square fa-3x"></span></a>',
    'linkedin' => '<a href=":url" class="social-button :class" id=":id" title=":title"><span class="fab fa-linkedin"></span></a>',
    'whatsapp' => '<a target="_blank" href=":url" class="social-button :class" id=":id" title=":title"><span class="fab fa-whatsapp"></span></a>',
    'pinterest' => '<a href=":url" class="social-button :class" id=":id" title=":title"><span class="fab fa-pinterest"></span></a>',
    'reddit' => '<a target="_blank" href=":url" class="social-button :class" id=":id" title=":title"><span class="fab fa-reddit"></span></a>',
    'telegram' => '<a target="_blank" href=":url" class="social-button :class" id=":id" title=":title"><span class="fab fa-telegram"></span></a>',
];
