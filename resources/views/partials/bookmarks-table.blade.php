<table id="bookmarks_table" class="table table-striped table-inverse table-responsive-sm">
    <thead class="thead-inverse">
        <tr>
            <th>Date</th>
            <th>Bookmark</th>
            <th>Share</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>


        @forelse($bookmarks as $bookmark)

        <tr>
            <td scope="row"> {{$bookmark->updated_at}}</td>


            <td>
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div id="bookmark_preview" class="text-center">
                                @foreach($bookmark->showLinkPreview() as $value)
                                {!! $value !!}
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-7">
                            <a class="uppercase" href="{{$bookmark->path()}}">{{ $bookmark->name }}</a>
                            <form action="{{$bookmark->path()}}" method="post">
                                @method('PUT')
                                @csrf

                                <input class="w-100" type="url" name="url" id="url-{{$bookmark->id}}" value="{{$bookmark->url}}">
                            </form>
                        </div>
                    </div>
                </div>





            </td>
            <td class="d-flex">
                {!! Share::page($bookmark->url, "$bookmark->name is one of my favourite website" , ['class' => 'sharable', 'id' => "share-$bookmark->id", 'title' => "$bookmark->name" ], '<ul class="list-unstyled d-flex justify-content-center">','</ul>')->facebook()->twitter() !!}

            </td>
            <td>


                <!-- Edit Button trigger modal -->
                <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#edit{{$bookmark->id}}">
                    Edit
                </button>

                <!-- Edit Modal -->
                <div class="modal fade" id="edit{{$bookmark->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-{{$bookmark->id}}" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Edit Bookmark </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p class="lead"> You are editing {{$bookmark->name}}</p>
                                <form id="edit-{{$bookmark->id}}" action="{{$bookmark->path()}}" method="post">
                                    @method('PUT')
                                    @csrf
                                    <div class="form-group">
                                        <label for="name">Bookmark Name</label>
                                        <input type="name" name="name" id="name-{{$bookmark->id}}" class="form-control" value="{{$bookmark->name}}" aria-describedby="nameHelper-{{$bookmark->id}}">
                                        <small id="nameHelper-{{$bookmark->id}}">Update the name for this bookmark</small>

                                    </div>
                                    <div class="form-group">
                                        <label for="url">Bookmark URL</label>
                                        <input type="url" name="url" id="url-{{$bookmark->id}}" class="form-control" value="{{$bookmark->url}}" aria-describedby="urlHelper-{{$bookmark->id}}">
                                        <small id="urlHelper-{{$bookmark->id}}">Update the URL for this bookmark</small>

                                    </div>
                                    <button form="edit-{{$bookmark->id}}" type="submit" class="btn btn-primary">Update</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>

                <!-- Delete Button trigger modal -->
                <button type="button" class="btn btn-lg" data-toggle="modal" data-target="#deleteBookmark-{{$bookmark->id}}">
                    <i class="fas fa-trash text-danger fa-lg"></i>
                </button>

                <!-- Delete Modal -->
                <div class="modal fade" id="deleteBookmark-{{$bookmark->id}}" tabindex="-1" role="dialog" aria-labelledby="bookmarkModal-{{$bookmark->id}}" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content text-center">
                            <div class="modal-header">
                                <h5 class="modal-title">Delete bookmark {{$bookmark->name}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure you want to delete this bookmark?</p>
                                <form id="delete-{{$bookmark->id}}" action="/bookmarks/{{$bookmark->id}}" method="post">
                                    @method('delete')
                                    @csrf
                                    <button form="delete-{{$bookmark->id}}" type="submit" class="btn btn-danger">Delete</button>

                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </td>
        </tr>


        @empty

        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>There are no bookmarks</strong>
        </div>



        @endforelse
    </tbody>
</table>