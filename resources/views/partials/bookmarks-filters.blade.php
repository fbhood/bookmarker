<nav id="filters-bar" class="d-flex justify-content-end py-4">

    <form action="/home" method="get">
        @csrf
        <input type="hidden" name="date_up" value="date_up">
        <button class="btn btn-primary mr-2" type="submit"><i class="fas fa-sort-up fa-lg fa-fw"></i>date</button>
    </form>
    <form action="/home" method="get">
        @csrf
        <input type="hidden" name="date_down" value="date_down">
        <button class="btn btn-primary mr-2" type="submit"><i class="fas fa-sort-down fa-lg fa-fw"></i>date</button>
    </form>
    <form action="/home" method="get">
        @csrf
        <input type="hidden" name="az" value="az">
        <button class="btn btn-primary mr-2" type="submit"><i class="fas fa-sort-alpha-down fa-lg fa-fw"></i></button>
    </form>
    <form action="/home" method="get">
        @csrf
        <input type="hidden" name="za" value="za">
        <button class="btn btn-primary" type="submit"><i class="fas fa-sort-alpha-up fa-lg fa-fw"></i></button>
    </form>


</nav>