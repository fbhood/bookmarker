<form id="create_bookmarks_form" action="/bookmarks" method="post">

    @csrf

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" id="name" aria-describedby="nameHelper" placeholder="example.com" required>
                    <small id="nameHelper" class="form-text text-muted">Add a name for your bookmark</small>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
                <div class="form-group">
                    <label for="url">URL</label>
                    <input type="url" class="form-control" name="url" id="url" aria-describedby="urlHelper" placeholder="https://example.com" required>
                    <small id="urlHelper" class="form-text text-muted">add a link to bookmark</small>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 d-flex flex-column justify-content-center">
                <button type="submit" class="btn btn-primary px-4">Save</button>
            </div>
        </div>
    </div>




</form>