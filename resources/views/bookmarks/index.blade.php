@extends('layouts.app')

@section('content')

<div class="jumbotron jumbotron-fluid bg-primary text-center">
    <div class="container">
        <h1 class="display-3">Bookmarks List</h1>

    </div>
</div>

<div class="container">
    <ul>
        @forelse($bookmarks as $bookmark)



        <li><a href="{{$bookmark->path()}}">{{$bookmark->name}}</a></li>


        @empty

        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>No Bookmars to show</strong>
        </div>
    </ul>
</div>
@endforelse


{{$bookmarks->links()}}
@endsection