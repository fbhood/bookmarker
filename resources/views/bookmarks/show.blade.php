@extends('layouts.app')

@section('content')



<header>
    <div class="container">

        <h1>{{$bookmark->name}} </h1>
        <a href="{{$bookmark->url}}">{{$bookmark->url}}</a>

        <p class="py-4"> <a class="btn btn-secondary rounded-pill" href="{{$bookmark->path()}}/edit" role="button">Edit</a>
            <a href="{{route('show-bookmarks')}}" role="button">Back</a>
        </p>
        <hr>
        <span>Share bookmark on: {!! Share::page($bookmark->url, "$bookmark->name is one of my favourite website" , ['class' => 'sharable', 'id' => "share-$bookmark->id", 'title' => "$bookmark->name" ], '<ul class="list-unstyled d-flex justify-content-start">','</ul>')->facebook()->twitter() !!}</span>

    </div>

</header>




@endsection