@extends('layouts.app')

@section('content')

<div class="container">

    <form action="{{$bookmark->path()}}" method="post">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="name">Bookmark Name</label>
            <input type="name" name="name" id="name" class="form-control" value="{{$bookmark->name}}" aria-describedby="nameHelper">
            <small id="nameHelper">Update the name for this bookmark</small>

        </div>
        <div class="form-group">
            <label for="url">Bookmark URL</label>
            <input type="url" name="url" id="url" class="form-control" value="{{$bookmark->url}}" aria-describedby="urlHelper">
            <small id="urlHelper">Update the URL for this bookmark</small>

        </div>
        <button type="submit" class="btn btn-primary">Update</button> <a href="/bookmarks">Cancel</a>
    </form>
</div>
@endsection