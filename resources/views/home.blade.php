@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card mb-5">
                <div class="card-header">Create Bookmarks</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @include('create-bookmarks-form')
                </div>
            </div>
            @include('partials.errors-alert')
            @if($bookmarks)
            <h3 class="section-title py-3">Saved Bookmarks</h3>
            @include('partials.bookmarks-filters')
            @include('partials.bookmarks-table')
            {{$bookmarks->links()}}
            @endif
        </div>
    </div>
</div>
@endsection