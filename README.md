## BookMarker

[![fbhood](https://circleci.com/bb/fbhood/Bookmarker.svg?style=svg)](https://app.circleci.com/bitbucket/fbhood/bookmarker/pipelines)

[![StyleCI](https://bitbucket.styleci.io/repos/-1638466868828318235/shield?branch=master)](https://bitbucket.styleci.io/repos/-1638466868828318235)

## About BookMarker

Bookmarker is a web application based on the [Laravel Framework](https://laravel.com). Users can store their bookmarks and see a preview of each url from an elegant UI. The link preview is generated via a third party API.

-   [Link Preview API - Free Registration ](https://www.linkpreview.net/).

## Installation

To install BookMarker your system needs to meet the Laravel system requirements, see the Laravel [documentation](https://laravel.com/docs) for more details.

1. Clone the repository
   The command below will clone the repository in a folder named Bookmarker

    ```
    git clone https://fbhood@bitbucket.org/fbhood/bookmarker.git Bookmarker
    ```

2. Create an .env file

    ```
    cd Bookmarker && cp .env.example .env
    ```

3. Generate a new app Key

    ```
    php artisan key:generate

    ```

4. Create a database for the application
   Create a database and make sure to grant complete access to the db to your mysql user

    ```
    mysql -u mysql-username-here -p

    create database laravel_bookmarker;
    exit;

    ```

5. Connect the database
   open the .env file and edit the database connection fields.
   Make sure to replace 'your-user-name-here' and 'your-password-here' with your db credentials

    ```
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=laravel_bookmarker
    DB_USERNAME=your-user-name-here
    DB_PASSWORD=your-password-here

    ```

6. Link Preview API Key
   Signup for a free account on [LinkPreview.net](https://www.linkpreview.net/)
   Then click Generate New Access Key in your user dashboard and copy the Key.

7. Paste the Link Preview Key
   Open your .env file and paste the LinkPreview Key that you copied in the previous step

    ```
    LINKPREVIEW_KEY=your-api-key-goes-here
    ```

8. Migrate your database
   inside the bookmarker project folder type the command below
    ```
    php artisan migrate
    ```

Depending on how you set up your envirnoment you can simply visit http://localhost in the browser or start the Laravel built in server if you need.

## Usage

To use the web application you need to register a user account first.
Then use the form on the top of the page to creata a bookmark and press the save button and you will see a preview of the bookmark saved.

-   **[Bookmarker demo](https://bookmarker.fabiopacifici.com/)**

## Contributing

Thank you for considering contributing to the BookMarker Application! There is no contribution guide available at the moment, feel free to report bugs or submit pull requests.

## Code of Conduct

The BookMarker code of conduct is derivated from the Laravel code of conduct [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability, please send an e-mail to Fabio via [fabiopacifici.com](https://fabiopacifici.com/contact-me/). All security vulnerabilities will be promptly addressed.

## License

This application is released under the [MIT license](https://opensource.org/licenses/MIT).
