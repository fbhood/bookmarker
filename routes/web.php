<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/bookmarks', 'BookmarkController@index')->name('show-bookmarks');
    Route::get('/bookmarks/{bookmark}', 'BookmarkController@show');

    Route::get('/bookmarks/create', 'BookmarkController@create');
    Route::post('/bookmarks', 'BookmarkController@store');
    Route::get('/bookmarks/{bookmark}/edit', 'BookmarkController@edit');
    Route::put('/bookmarks/{bookmark}', 'BookmarkController@update');
    Route::delete('/bookmarks/{bookmark}', 'BookmarkController@destroy');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
