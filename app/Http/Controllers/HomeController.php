<?php

namespace App\Http\Controllers;

use App\Bookmark;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Jorenvh\Share\ShareFacade;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Bookmark $bookmark, User $user)
    {
        if (request()->has('date_up')) {
            $bookmarks = auth()->user()->bookmarks()->orderBy('updated_at', 'asc')->paginate(10);

            return view('home', compact('bookmarks'));
        }
        if (request()->has('date_down')) {
            $bookmarks = auth()->user()->bookmarks()->orderBy('updated_at', 'desc')->paginate(10);

            return view('home', compact('bookmarks'));
        }
        if (request()->has('az')) {
            $bookmarks = auth()->user()->bookmarks()->orderBy('name', 'asc')->paginate(10);

            return view('home', compact('bookmarks'));
        }
        if (request()->has('za')) {
            $bookmarks = auth()->user()->bookmarks()->orderBy('name', 'desc')->paginate(10);

            return view('home', compact('bookmarks'));
        }
        $bookmarks = auth()->user()->bookmarks()->orderBy('name')->paginate(10);

        return view('home', compact('bookmarks'));
    }
}
