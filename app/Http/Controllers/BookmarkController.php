<?php

namespace App\Http\Controllers;

use App\Bookmark;
use App\User;
use Illuminate\Http\Request;

class BookmarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookmarks = auth()->user()->bookmarks()->paginate(10);

        return view('bookmarks.index', compact('bookmarks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Bookmark $bookmark, User $user)
    {
        //
        $bookmarks = $user->bookmarks;
        //dd($bookmarks);
        return view('home', compact('bookmarks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        //

        $request->validate([
            'name' => 'required',
            'url' => 'required | active_url | starts_with:https://',
        ]);

        Bookmark::create([
            'user_id' => auth()->user()->id,
            'name' => request('name'),
            'url' => request('url'),
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function show(Bookmark $bookmark)
    {
        if (auth()->id() !== $bookmark->user_id) {
            abort(403);
        }

        return view('bookmarks.show', compact('bookmark'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function edit(Bookmark $bookmark)
    {
        return view('bookmarks.edit', compact('bookmark'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bookmark $bookmark)
    {
        if (request()->has('name')) {
            $bookmark->update(['name' => $request->name]);
        }
        if (request()->has('url')) {
            $bookmark->update(['url' => $request->url]);
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bookmark $bookmark)
    {
        //

        $bookmark->delete();

        return back();
    }
}
