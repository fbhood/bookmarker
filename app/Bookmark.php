<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function path()
    {
        return "/bookmarks/$this->id";
    }

    public function showLinkPreview()
    {
        try {
            $target = urlencode($this->url);
            $key = config('app.link_preview_api_key');
            $resp = file_get_contents("https://api.linkpreview.net?key={$key}&q={$target}");
            $previewArray = json_decode($resp, true); // returns an array
            /*  $previewArray = [
                'title' => 'lorem ipsum link',
                'description' => 'lorem ipsum dolor si tamer',
                'image' => 'https://fabiopacifici.com/wp-content/uploads/2017/06/cropped-fabio-pacifici-roi-seo-sem.png'
            ]; */

            if (array_key_exists('image', $previewArray)) {
                $preview[] = '<a href="'.$this->url.'"> <img class="img-fluid preview-image" width="100" src=" '.$previewArray['image'].'"></a>';
            }
            if (array_key_exists('title', $previewArray)) {
                $preview[] = '<h5 class="preview-title">'.substr($previewArray['title'], 0, 15).'</h5>';
            }
            if (array_key_exists('description', $previewArray)) {
                $preview[] = '<p class="preview-desc">'.substr($previewArray['description'], 0, 50).'</p>';
            }

            return $preview;
        } catch (\Throwable $th) {
            return $th;
        }
    }
}
